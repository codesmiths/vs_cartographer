// Define a function to hide/show overlays. Returns the negated
// state.
vcs_toggle_overlays = function(turn_on, menu_class, ol_class) {
	// switch the menu indicator on/off
	var m = document.querySelector(menu_class);
	m.style.opacity = turn_on ? '0.3' : '1.0';
	var elements = document.getElementsByClassName(ol_class);
	for (var i = 0; i < elements.length; i++) {
	    if (turn_on) {
		elements[i].style.visibility = 'hidden';
	    }
	    else {
		elements[i].style.visibility = 'visible';
	    }
	}
	return !turn_on;

};

var wp_visible_trader = false;
var wp_visible_bees = false;
var wp_visible_res = false;
var wp_visible_spawn = true;
var wp_visible_tl = true;
var wp_visible_home = true;
var wp_visible_poi = true;
var key_visible = false;

vs_cartographer_setup_menu = function() {
	// turn traders off by default
	vcs_toggle_overlays(true, '#toggle_trader', 'o_trader');

	document.querySelector('#toggle_trader').addEventListener('click', function() {
	    wp_visible_trader = vcs_toggle_overlays(wp_visible_trader, '#toggle_trader', 'o_trader');
	});

	// turn bees off by default
	vcs_toggle_overlays(true, '#toggle_bees', 'o_bees');

	document.querySelector('#toggle_bees').addEventListener('click', function() {
	    wp_visible_bees = vcs_toggle_overlays(wp_visible_bees, '#toggle_bees', 'o_bees');
	});

	// turn resources off by default
	vcs_toggle_overlays(true, '#toggle_res', 'o_resource');

	document.querySelector('#toggle_res').addEventListener('click', function() {
	    wp_visible_res = vcs_toggle_overlays(wp_visible_res, '#toggle_res', 'o_resource');
	});

	document.querySelector('#toggle_spawn').addEventListener('click', function() {
	    wp_visible_spawn = vcs_toggle_overlays(wp_visible_spawn, '#toggle_spawn', 'o_spawn');
	});

	document.querySelector('#toggle_tl').addEventListener('click', function() {
	    wp_visible_tl = vcs_toggle_overlays(wp_visible_tl, '#toggle_tl', 'o_translocator');
	});

	document.querySelector('#toggle_home').addEventListener('click', function() {
	    wp_visible_home = vcs_toggle_overlays(wp_visible_home, '#toggle_home', 'o_home');
	});

	document.querySelector('#toggle_pois').addEventListener('click', function() {
	    wp_visible_poi = vcs_toggle_overlays(wp_visible_poi, '#toggle_pois', 'o_poi');
	});

	// the color key (legend)
	// attach a handler to slide in/out the color key
	document.querySelector('#sbslide').addEventListener('click', function() {
	    var element = document.getElementById('map_key');
	    var slider = document.getElementById('sbslider');
	    if (key_visible) {
		element.style.visibility = 'hidden';
		element.style.display = 'none';
		slider.innerHTML = '&#65086;';
	    }
	    else {
		element.style.visibility = 'visible';
		element.style.display = 'inline-block';
		slider.innerHTML = '&#65085;';
	    }
	 key_visible = !key_visible;
	});
};
