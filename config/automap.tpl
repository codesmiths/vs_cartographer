<!DOCTYPE html><html><head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="http://bloodgate.com/vintagestory/css/automap.css">
    <script type="text/javascript" src="http://bloodgate.com/vintagestory/js/openseadragon.min.js"></script>
    <script type="text/javascript" src="http://bloodgate.com/vintagestory/js/openseadragon-bookmark-url.js"></script>
    <script type="text/javascript" src="http://bloodgate.com/vintagestory/js/vs_cartographer.js"></script>
    <title>##title##</title>
</head>
<body>
    ##menu##
    <div id="mapcontainer">
        <div id="map"></div>
        <div id="coordinates">0, 0, 0</div>
    </div>
    <div id="notice">
        (c) Tels ##date## &ndash;
        Made with <a href="https://osdn.net/projects/automap/releases/">VS Automap</a> &nbsp;-&nbsp;
        <a href="https://gitlab.com/codesmiths/vs_cartographer">VS Cartographer</a> &nbsp;-&nbsp;
        <a href="https://openseadragon.github.io/">OpenSeadragon</a>
    </div>
   ##menu##
   ##viewer##
</body>
</html>
